/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.stericson.RootTools.RootTools;

public class MainActivity extends Activity {

    private SeekBar redBar;
    private SeekBar greenBar;
    private SeekBar blueBar;

    private Color oldColor = new Color();
    private Color newColor = new Color();

    private boolean isEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!(RootTools.isRootAvailable() && RootTools.isAccessGiven())){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Root Access")
                    .setMessage("Please Give Root Access and Try again")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            builder.create();
            builder.setCancelable(false);
            builder.show();
        }
        else {
            setContentView(R.layout.dialog);

            isEnabled = SysFS.readCtrl();

            Button bOk = (Button) findViewById(R.id.ok);
            Button bCancel = (Button) findViewById(R.id.cancel);

            TextView redPercent = (TextView) findViewById(R.id.rp);
            TextView greenPercent = (TextView) findViewById(R.id.gp);
            TextView bluePercent = (TextView) findViewById(R.id.bp);

            redBar = (SeekBar) findViewById(R.id.rsb);
            greenBar = (SeekBar) findViewById(R.id.gsb);
            blueBar = (SeekBar) findViewById(R.id.bsb);

            final Switch ctrlSwitch = (Switch) findViewById(R.id.sw);

            if(isEnabled){
                ctrlSwitch.setChecked(true);
            }
            else{
                ctrlSwitch.setChecked(false);
                redBar.setEnabled(false);
                greenBar.setEnabled(false);
                blueBar.setEnabled(false);
            }

            redBar.setMax(Data.maxValue-Data.minValue);
            greenBar.setMax(Data.maxValue-Data.minValue);
            blueBar.setMax(Data.maxValue-Data.minValue);

            oldColor = SysFS.readColor();
            newColor = SysFS.readColor();

            if(oldColor.isBlack()){
                oldColor = new Color();
                SysFS.write(oldColor);
            }

            int[] currColor = oldColor.getColors();

            redBar.setProgress(currColor[0] - Data.minValue);
            updatePercent(redPercent, currColor[0] - Data.minValue);
            greenBar.setProgress(currColor[1] - Data.minValue);
            updatePercent(greenPercent,currColor[1] - Data.minValue);
            blueBar.setProgress(currColor[2] - Data.minValue);
            updatePercent(bluePercent, currColor[2] - Data.minValue);

            bOk.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    oldColor = newColor;
                    isEnabled = ctrlSwitch.isChecked();

                    PreferenceManager
                            .getDefaultSharedPreferences(getApplicationContext()).edit()
                            .putInt(Data.SPKeys.RED_SP,newColor.getRed())
                            .putInt(Data.SPKeys.GREEN_SP,newColor.getGreen())
                            .putInt(Data.SPKeys.BLUE_SP,newColor.getBlue())
                            .putBoolean(Data.SPKeys.ENABLED_SP,isEnabled)
                            .apply();

                    finish();
                }
            });

            bCancel.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            ctrlSwitch.setOnCheckedChangeListener(swListener());

            redBar.setOnSeekBarChangeListener(sbListener(redPercent));
            greenBar.setOnSeekBarChangeListener(sbListener(greenPercent));
            blueBar.setOnSeekBarChangeListener(sbListener(bluePercent));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(!oldColor.isBlack()) SysFS.write(oldColor);
        else SysFS.write(new Color());

        SysFS.write(isEnabled);
    }

    private SeekBar.OnSeekBarChangeListener sbListener(final TextView tvPercent){
        return new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int iColor = progress + Data.minValue;
                updatePercent(tvPercent,progress);
                if(seekBar.getId()==R.id.rsb){
                    newColor.setRed(iColor);
                }
                else if(seekBar.getId()==R.id.gsb){
                    newColor.setGreen(iColor);
                }
                else if(seekBar.getId()==R.id.bsb){
                    newColor.setBlue(iColor);
                }
                SysFS.write(newColor);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        };
    }

    private Switch.OnCheckedChangeListener swListener(){
        return new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SysFS.write(isChecked);
                if(!isChecked){
                    redBar.setEnabled(false);
                    greenBar.setEnabled(false);
                    blueBar.setEnabled(false);
                }
                else {
                    redBar.setEnabled(true);
                    greenBar.setEnabled(true);
                    blueBar.setEnabled(true);
                }
            }
        };
    }

    private void updatePercent(TextView percentTV, int progress){
        float iPercent = progress * 100 / (Data.maxValue - Data.minValue);
        String percent = (int)iPercent + "%";
        percentTV.setText(percent);
    }

    public void infoButton(View view) {
        startActivity(new Intent(MainActivity.this, About.class));
    }
}
